# jssh

#### 项目介绍
linux scp(文件上传，下载) shell命令的java ssh工具,主要是避免shell 密码手工输入带来的麻烦。 

#### 软件架构
jssh.jar 使用jsch封装的java版本<br/>
jssh.sh 使用expect封装的sh版本<br/>
<br/>
<em>以上两者是独立的，不同的实现。</em><br/>

#### jssh.jar安装教程

<pre>
wget https://gitee.com/chejiangyi/jssh/raw/master/release/master/jssh.jar
</pre>

#### jssh.jar使用说明

<b>命令格式:</b>java -jar jssh.jar [用户名]@[ip]:[端口]@[密码] [命令] [命令参数...]<br/>
<b>说明:</b><br/>
[用户名] root<br/>
[ip]10.200.133.69<br/>
[端口]22<br/>
[密码]cjy@@2018! （若@为转义，使用@@）<br/>
[命令]sftp （枚举:shell,sftp）<br/>
[参数]/root/setting.xml /root/setting.xml (根据命令指定参数)<br/><br/>
<i>shell命令:</i><br/>
支持 linux shell<br/>
exit (表示退出shell)<br/><br/>
<i>sftp命令:</i><br/>
sftp [get(下载)/put(上传)] [来源文件路径 [目标文件路径]<br/>
<br/>
<i>示范1(上传,下载同理):</i><br/>
<pre><code>java -jar jssh.jar root@10.200.133.69:22@123456 sftp put c:/setting.xml /root/setting.xml</code></pre>
![img1](http://images2018.cnblogs.com/blog/815689/201806/815689-20180622100140266-1620257899.png)<br/>
<i>示范2(shell):</i><br/>
<pre>java -jar jssh.jar root@10.200.133.69:22@123456 shell &lt;&lt;EOF  
cd /
cat stream-mysql.log
exit
EOF</pre>
![img2](http://images2018.cnblogs.com/blog/815689/201806/815689-20180622101409578-253175765.png)<br/>
#### jssh.sh安装教程
<pre>
yum install expect
</pre>
<pre>
wget https://gitee.com/chejiangyi/jssh/raw/master/release/master/jssh.sh
</pre>

#### jssh.sh使用说明

<b>命令格式:</b>expect jssh.sh [命令] [用户名]@[ip]:[fromfile] [tofile] [remotepassword]<br/><br/>
<i>scp命令(下载):</i> expect jssh.sh scp [用户名]@[ip]:[fromfile] [tofile] [remotepassword]<br/>
<i>scp命令(上传):</i> expect jssh.sh scp [fromfile] [用户名]@[ip]:[tofile] [remotepassword]<br/>
举例:<pre>expect jssh.sh scp root@10.200.133.50:tt.sql /tt.sql 111@2017!</pre>
![img3](http://images2018.cnblogs.com/blog/815689/201806/815689-20180622101936261-468046790.png)<br/>
<i>ssh命令:</i>expect jssh.sh ssh [用户名]@[ip] [remotepassword]<br/>
举例:<pre>expect jssh.sh ssh root@10.200.133.50 111@2017!</pre>
![img4](http://images2018.cnblogs.com/blog/815689/201806/815689-20180622102117601-1920746510.png)<br/>
#### 参与贡献

by 车江毅


